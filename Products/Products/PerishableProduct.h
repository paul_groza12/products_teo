#pragma once
#include "Product.h"

class PerishableProduct :public Product
{
public:
	PerishableProduct(int32_t id, const std::string& name, float rawPrice, const std::string& expirationDate);

	std::string getExpirationDate() const;
	float getPrice() const final;
	int32_t getVAT() const final;

private:
	std::string m_expirationDate;
};