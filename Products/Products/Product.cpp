#include "Product.h"

Product::Product(int32_t id, const std::string& name, float rawPrice) : m_id(id), m_name(name), m_rawPrice(rawPrice)
{
	// empty
}

#pragma region Getters
uint16_t Product::getID() const
{
	return m_id;
}
std::string Product::getName() const
{
	return m_name;
}
float Product::getRawPrice() const
{
	return m_rawPrice;
}
#pragma endregion Getters